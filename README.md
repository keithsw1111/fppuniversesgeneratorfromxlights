# README #

To use the program just run the executable.

Browse for your XLights show directory.

Click generate to generate the FPP universes file data

Click copy to copy it into the clipboard

In putty log onto your pi ... go to the media folder

backup your current universes file ... cp universes universes.backup

using nano open the universes file ... nano universes

press CTRL K to delete all the existing data

now click right mouse button to paste in the new content.

CTRL O then CTRL X will save and exit.

Now restart Falcon (you may need to navigate away from the channel outputs screen to get it to refresh.

### What is this repository for? ###

* Small utility for generating falcon pi player universes configuration from XLights
* 1.0

### How do I get set up? ###

This should be compilable with the latest version of Microsofts free version of visual studio.

### Contribution guidelines ###

* Happy to access suggestions for improvement or grant others access to improve it.

### Who do I talk to? ###

* keithsw1111@hotmail.com