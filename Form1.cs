﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace FPPUniversesGeneratorFromXlights
{
    public partial class Form1 : Form
    {
        void ValidateWindow()
        {
            if (Directory.Exists(textBox1.Text) && File.Exists(textBox1.Text + "\\xlights_networks.xml"))
            {
                button2.Enabled = true;
            }
            else
            {
                button2.Enabled = false;
            }

            if (textBox2.Text == string.Empty)
            {
                button3.Enabled = false;
            }
            else
            {
                button3.Enabled = true;
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Generate
            textBox2.Text = string.Empty;
            XmlDocument doc = new XmlDocument();
            doc.Load(textBox1.Text + "\\xlights_networks.xml");
            int count = 1;

            foreach (XmlNode n in doc.ChildNodes)
            {
                if (n.Name.ToLower() == "networks")
                {
                    foreach (XmlNode n1 in n.ChildNodes)
                    {
                        if(n1.Name.ToLower() == "network")
                        {
                            if (n1.Attributes["NetworkType"].Value == "E131")
                            {
                                string ip = n1.Attributes["ComPort"].Value;
                                string universe = n1.Attributes["BaudRate"].Value;
                                string channels = n1.Attributes["MaxChannels"].Value;
                                int chan = Int16.Parse(channels);
                                int end = count + chan - 1;
                                if (ip == "MULTICAST")
                                {
                                    // I am guessing this one ... I have left off the ip address
                                    textBox2.Text = textBox2.Text + "1," + universe + "," + count.ToString().Trim() + "," + chan.ToString().Trim() + ",0,,\r\n";
                                }
                                else
                                {
                                    textBox2.Text = textBox2.Text + "1," + universe + "," + count.ToString().Trim() + "," + chan.ToString().Trim() + ",1," + ip + ",\r\n";
                                }
                                count = end + 1;
                            }
                        }
                    }
                }
            }
            ValidateWindow();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ValidateWindow();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Browse
            if (folderBrowserDialog1.ShowDialog(this) == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
            ValidateWindow();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Copy
            Clipboard.SetData(DataFormats.Text, textBox2.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ValidateWindow();
        }
    }
}
